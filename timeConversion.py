s = '06:40:03AM'
def timeConversion(s):
    #input is s which is a string in 12hr time format
    #output is string in 24hr time format
    if s[-2:] == 'PM':
        if s[:2] != '12':
            two_four = str(int(s[:2]) + 12) + s[2:-2]
        else:
            two_four = s[:-2]
    else:
        if s[:2] == '12':
            two_four = '00' + s[2:-2]
        else:
            two_four = s[:-2]
    return two_four


    #check if the time is in AM or PM
    # if PM add 12 to the hour
    # if AM do nothing to the hour


print(timeConversion(s))