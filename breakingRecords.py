import math

scores = (17, 45, 41, 60, 17, 41, 76, 43, 51, 40, 89, 92, 34, 6, 64, 7, 37, 81, 32, 50)

def breakingRecords(scores):
    min_score = scores[0]
    max_score = scores[0]
    sum_min = 0
    sum_max = 0

    if scores == 0:
        return (sum_max, sum_min)

    for score in scores:
        if score < min_score:
            min_score = score
            sum_min += 1
        if score > max_score:
            max_score = score
            sum_max += 1
    return (sum_max, sum_min)

print(breakingRecords(scores))
