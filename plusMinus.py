import math

arr = [-4, 3, -9, 0, 4, 1]

def plusMinus(arr):
    # input is list of numbers
    # output: 3 lines of the amount of zeros, pos, and neg nums each divided by the size
    # of the total array
    # 0.500000
    # 0.333333
    # 0.166667

    # unpack the arr find see if it is pos, neg, or zero
    list_len = len(arr)
    neg_count = 0
    pos_count = 0
    zero_count = 0

    for num in arr:
        if num > 0:
            pos_count += 1
        if num < 0:
            neg_count +=1
        if num == 0:
            zero_count += 1
    print("{:.6f}".format(pos_count/list_len))
    print(float("{:.6f}".format(neg_count/list_len)))
    print(float("{:.6f}".format(zero_count/list_len)))
plusMinus(arr)