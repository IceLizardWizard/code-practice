import math

arr = [1, 2, 3, 4, 5]

def mini_max_sum(arr):
    #input arr is a list of numbers
    #output is the min and max sums of the list minus min for max and max for min
    #find the min of the list and remove it
    #find the max of the list and remove it
    #sum the list of remaining
    #add the min to the sum of the list
    #add the max to the sum of the list
    new_arr = arr
    max_num = new_arr.pop(arr.index(max(arr)))
    min_num = new_arr.pop(arr.index(min(arr)))
    print(min_num + sum(new_arr), max_num + sum(new_arr))
print(mini_max_sum(arr))
